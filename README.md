# lilHarv

This is a simple 8-bit cpu with seperated instruction and data memory (harvard architecture) written in [PSHDL](http://pshdl.org/pshdl.html).

It's based on the introduction lecture to [Computer Architecture](http://intranet.tuhh.de/kvvz/vorlesung.php3?Lang=en&sg_s=&id=key2219) where VHDL was used. As an learning excercises i reimplemented it in PSHDL.

I also added the slides of the first lecture where you can find the instruction set and the registers (the stack isn't implemented yet).