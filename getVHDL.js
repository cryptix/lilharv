var request = require('request'),
		fs = require('fs');

var wid = "E1CCD855F51C73F4";

request('http://api.pshdl.org:9998/api/v0.1/workspace/' + wid, function (error, response, body) {
  if (!error && response.statusCode == 200) {
		var jsonbody = JSON.parse(body);
    // console.dir(jsonbody);
    jsonbody.files.forEach(function(file,i) {
			if (file['@syntax'] === "ok") {
				console.log('saving vhdl of ' + file['@name']);
				var targetfname = file['@name'].replace('pshdl', 'vhd');
				fs.writeFileSync(targetfname, file.info.source);
			}
    });
  }
});